# -*- coding: utf-8 -*-


from os import path, makedirs, remove, listdir, walk, unlink
from sys import platform
from json import load, dump, dumps
from app.lib.flgit import git_operation as g
basedir = path.abspath(path.dirname(__file__))
import logging, shutil
from logging.config import dictConfig
from distutils.util import strtobool
from gelfHandler import GelfHandler
from app.lib.utils import *
from logging_gelf import handlers
from logging_gelf.formatters import GELFFormatter


def check_path(path_dir):
    if not path.exists(path_dir):
        makedirs(path_dir)

def read_file(file_path):
    with open(file_path, 'r') as file:
        data = file.read().replace('\n', '')
        file.close()
    return data

def load_conf(file_name):
    if not path.isfile(file_name):
        print('Config not found! Create new.')
        data = {
            "systems": {},
            "app":{
                "SECRET_KEY": 'OCML3BRawWEUeaxcuKHLpw',
                "CSRF": True,
                "DEBUG": False,
                "SESSION_COOKIE_SECURE": False,
                "ENV": "production",
                "MAX_CONTENT_LENGTH": 50 * 1024 * 1024,
                "ALLOWED_FILE_EXTENSIONS": ["json", "txt", "html", "conf", "sub"],
                "repository_dir": "repository",
                "cred_file_dir": "/run/secrets",
                "loginig": {
                    "log_dir": "logs",
                    "log_file_name": "CCS.log",
                    "log_level": "INFO",
                    "log_max_size": 1024,
                    "log_backup_count": 3
                },
                "git_settings": {
                    "git_log_level": "INFO",
                    "git_user": "WebOnline Git-user",
                    "git_mail": "git-user.webonline@homecredit.ru"
                },
                "glef": {
                "host": "0.0.0.0",
                "port": 12202,
                "protocol": 'udp'
                },
                "creds": {}
            }
        }
        with open(file_name, 'w') as f:
            dump(data, f)
    else:
        print("Config found.")
    with open(file_name) as f:
        conf = load(f)
    return conf



def check_repository(pathx, conf, log_conf, logger, git, cred):
    repository = path.join(pathx, conf['app']['repository_dir'])
    #check_path(repository)
    systems_dirs = []
    for system in  conf['systems']:
        logger.info(dumps({
        'system': system, 'action': "check_repository", 
        'message': "Check system: {}".format(system),
        'value': None}, sort_keys=True, indent=4))
        if not "enabled" in conf['systems'][system]:
            conf['systems'][system]["enabled"] = True
            logger.warning(dumps({
            'system': system, 'action': "check_repository.chekConfig.enabled", 
            'message': "Not found parametr [enabled] for system: {} set [True]".format(system),
            'value': None}, sort_keys=True, indent=4))
        if conf['systems'][system]["enabled"]:
            logger.debug(dumps({
            'system': system, 'action': "check_repository.isSystemEnabled", 
            'message': "System: {} is: Enabled".format(system),
            'value': True}, sort_keys=True, indent=4))
            systems_dirs.append(conf['systems'][system]["git"]["work_dir"])
            DIR_NAME = path.join(repository, conf['systems'][system]['git']['work_dir'])
            logger.debug(dumps({
            'system': system, 'action': "check_repository.chekConfig.work_dir", 
            'message': "{} path: {}".format(system, DIR_NAME),
            'value': DIR_NAME}, sort_keys=True, indent=4))
            gc = {}
            gc.update( conf['systems'][system]['git'])
            gc["DIR_NAME"] = DIR_NAME
            gc['log_conf'] = log_conf
            gc['cred_file'] = conf['app']['creds'][conf['systems'][system]['git']['cred_id']]
            gc['cred_id'] = conf['app']['creds'][conf['systems'][system]['git']['cred_id']]
            gc['sys_user'] = git
            go = g(gc, cred)
            if path.exists(gc['DIR_NAME']) and path.isdir(gc['DIR_NAME']):
                logger.debug(dumps({
                'system': system, 'action': "check_repository.chekWorkDir", 
                'message': "{} {}".format(system, "Path exists"),
                'value': DIR_NAME}, sort_keys=True, indent=4))
                if not listdir(gc['DIR_NAME']):
                    logger.debug(dumps({
                    'system': system, 'action': "check_repository.chekWorkDir", 
                    'message': "{} {}".format(system, "Path is empty"),
                    'value': DIR_NAME}, sort_keys=True, indent=4))
                    if strtobool(gc['auto_recreate']):
                        logger.debug(dumps({
                        'system': system, 'action': "check_repository.chekConfig.auto_recreate", 
                        'message': "{} auto_recreate: {}".format(system, gc['auto_recreate']),
                        'value': gc['auto_recreate']}, sort_keys=True, indent=4))
                        try:
                            logger.debug(dumps({
                            'system': system, 'action': "check_repository.Clean", 
                            'message': "{} try: {}".format(system, "clean"),
                            'value': ""}, sort_keys=True, indent=4))
                            shutil.rmtree(gc['DIR_NAME'])
                        except Exception as e:
                            logger.error(dumps({
                            'system': system, 'action': "check_repository.Clean", 
                            'message': '{} Failed to delete {}.'.format(system, gc['DIR_NAME']),
                            'value': e}, sort_keys=True, indent=4))
                        finally:
                            logger.debug("{} try: {}".format(system, "clone"))
                            logger.debug(dumps({
                            'system': system, 'action': "check_repository.Clone", 
                            'message': "{} try: {}".format(system, "clone"),
                            'value': ""}, sort_keys=True, indent=4))
                            go.clone(gc['branch'])
                else:
                    logger.debug(dumps({
                    'system': system, 'action': "check_repository.chekWorkDir", 
                    'message': "{} {}".format(system, "Directory is not empty"),
                    'value': DIR_NAME}, sort_keys=True, indent=4))
                    logger.debug(dumps({
                    'system': system, 'action': "check_repository.chekConfig.auto_recreate", 
                    'message': "{} auto_recreate: {}".format(system, gc['auto_recreate']),
                    'value': gc['auto_recreate']}, sort_keys=True, indent=4))

                    if strtobool(gc['auto_recreate']):
                        logger.debug(dumps({
                        'system': system, 'action': "check_repository.chekExistsDir.git", 
                        'message': "{} .git: {}".format(system, path.exists(path.join(gc['DIR_NAME'],".git"))),
                        'value': path.exists(path.join(gc['DIR_NAME'],".git"))}, sort_keys=True, indent=4))
                        if not platform.startswith('win') or not path.exists(path.join(gc['DIR_NAME'],".git")):
                            for filename in listdir(gc['DIR_NAME']):
                                file_path = path.join(gc['DIR_NAME'], filename)
                                try:
                                    logger.debug(dumps({
                                    'system': system, 'action': "check_repository.Clean", 
                                    'message': "{} try: {}".format(system, "clean"),
                                    'value': ""}, sort_keys=True, indent=4))
                                    shutil.rmtree(gc['DIR_NAME'])
                                    if path.isfile(file_path) or path.islink(file_path):
                                        unlink(file_path)
                                    elif path.isdir(file_path):
                                        shutil.rmtree(file_path)
                                except Exception as e:
                                    logger.error(dumps({
                                    'system': system, 'action': "check_repository.Clean", 
                                    'message': '{} Failed to delete {}.'.format(system, gc['DIR_NAME']),
                                    'value': "{}".format(e)}, sort_keys=True, indent=4))
                            try:
                                logger.debug(dumps({
                                'system': system, 'action': "check_repository.Clone", 
                                'message': "{} try: {}".format(system, "clone"),
                                'value': ""}, sort_keys=True, indent=4))
                                go.clone(gc['branch'])
                            except Exception as e:
                                logger.error(dumps({
                                'system': system, 'action': "check_repository.Clone", 
                                'message': '{} Failed to delete {}.'.format(system, gc['DIR_NAME']),
                                'value': e}, sort_keys=True, indent=4))
                        else:
                            logger.debug(dumps({
                            'system': system, 'action': "check_repository.headReset", 
                            'message': "{} try: {}".format(system, "head_reset"),
                            'value': ""}, sort_keys=True, indent=4))
                            go.head_reset()
                            logger.debug(dumps({
                            'system': system, 'action': "check_repository.pull", 
                            'message': "{} try: {}".format(system, "pull"),
                            'value': ""}, sort_keys=True, indent=4))
                            go.pull()
                    else:
                        logger.debug(dumps({
                        'system': system, 'action': "check_repository.fetch", 
                        'message': "{} try: {}".format(system, "fetch"),
                        'value': ""}, sort_keys=True, indent=4))
                        go.fetch()
                        logger.debug(dumps({
                        'system': system, 'action': "check_repository.chekConfig.auto_pull", 
                        'message': "{} auto_pull: {}".format(system, gc['auto_pull']),
                        'value': gc['auto_pull']}, sort_keys=True, indent=4))
                        if strtobool(gc['auto_pull']):
                            logger.debug(dumps({
                            'system': system, 'action': "check_repository.pull", 
                            'message': "{} try: {}".format(system, "pull"),
                            'value': ""}, sort_keys=True, indent=4))
                            go.pull()
            else:
                logger.debug(dumps({
                'system': system, 'action': "check_repository.chekWorkDir", 
                'message': "Given Directory don't exists",
                'value': DIR_NAME}, sort_keys=True, indent=4))
                logger.debug(dumps({
                'system': system, 'action': "check_repository.Clone", 
                'message': "{} try: {}".format(system, "clone"),
                'value': ""}, sort_keys=True, indent=4))
                go.clone()
            logger.info(dumps({
            'system': system, 'action': "check_repository", 
            'message': "Check system: {} is: done".format(system),
            'value': None}, sort_keys=True, indent=4))
        else:
            logger.debug(dumps({
            'system': system, 
            'action': "check_repository.isSystemEnabled", 
            'message': "System: {} is: Disabled".format(system),
            'value': False}, sort_keys=True, indent=4))
    return repository


class Config(object):
    APPLICATION_ROOT='/'
    ROOT_DIR = basedir
    APP = path.join(basedir, 'app')
    STATIC = path.join(APP,'static')
    UPLOADS =  path.join(STATIC, "uploads")
    check_path(UPLOADS)
    CONFIG_PATH = path.join(UPLOADS, 'ccs')
    check_path(CONFIG_PATH)
    APP_CONFIG_FILE = path.join(CONFIG_PATH, 'config.json')
    APP_CONFIG = load_conf(APP_CONFIG_FILE)
    CREDS = {}
    for cred in APP_CONFIG["app"]["creds"]:
        CREDS["{}".format(APP_CONFIG["app"]["creds"][cred]["name"])] = {
            "name": APP_CONFIG["app"]["creds"][cred]["name"],
            "login": APP_CONFIG["app"]["creds"][cred]["login"],
            "passwd": read_file(APP_CONFIG["app"]["creds"][cred]["cred_file"])
        }
    SESSION_TYPE = 'filesystem'
    

class DefaultConfig(Config):
    DEBUG = Config.APP_CONFIG["app"]["DEBUG"]
    CSRF_ENABLED = Config.APP_CONFIG["app"]["CSRF"]
    SECRET_KEY = Config.APP_CONFIG["app"]["SECRET_KEY"]
    SESSION_COOKIE_SECURE = Config.APP_CONFIG["app"]["SESSION_COOKIE_SECURE"]
    ALLOWED_FILE_EXTENSIONS = Config.APP_CONFIG["app"]["ALLOWED_FILE_EXTENSIONS"]
    MAX_CONTENT_LENGTH = Config.APP_CONFIG["app"]["MAX_CONTENT_LENGTH"] * 1024 * 1024 #50MB
    GIT = { "user": Config.APP_CONFIG["app"]["git_settings"]["git_user"],
            "mail": Config.APP_CONFIG["app"]["git_settings"]["git_mail"]
        }
    GIT_LOG =  Config.APP_CONFIG["app"]["git_settings"]["git_log_level"]
    LOG_DIR_NAME = Config.APP_CONFIG["app"]["loginig"]["log_dir"]
    LOG_DIR_PATH = path.join(basedir, path.join(path.join('app','static'), LOG_DIR_NAME))
    LOG_FILE_NAME = Config.APP_CONFIG["app"]["loginig"]["log_file_name"]
    LOG_LEVEL =  Config.APP_CONFIG["app"]["loginig"]["log_level"]
    LOG_MAX = Config.APP_CONFIG["app"]["loginig"]["log_max_size"]
    LOG_BACKUP_COUNT = Config.APP_CONFIG["app"]["loginig"]["log_backup_count"]
    check_path(LOG_DIR_PATH)
# "gelfHandler": {
#                 "class":"gelfHandler.GelfHandler",
#                 "host": '10.6.155.104',
#                 "port": 7020,
#                 "protocol": 'UDP'
#             },
#GELFUDPSocketHandler(host="127.0.0.1", port=12202)
#logging-gelf-0.0.26
    LOG_CONF = {
        "version":1,
        "handlers":{
            # "gelfHandler": {
            #     "class":"logging_gelf.handlers.GELFUDPSocketHandler",
            #     "host": '127.0.0.1',
            #     "port": 7020,
            #     "formatter": "GELFFormatter"
            # },
    

            "fileHandler":{
                "class":"logging.handlers.RotatingFileHandler",
                "formatter": "{}{}".format("CCSFormatter",LOG_LEVEL),
                "filename": "{}".format(path.join(LOG_DIR_PATH, LOG_FILE_NAME)),
                "maxBytes": LOG_MAX,
                "backupCount": LOG_BACKUP_COUNT
            },
            "fileHandlerGit":{
                "class":"logging.handlers.RotatingFileHandler",
                "formatter": "{}{}".format("CCSFormatter",LOG_LEVEL),
                "filename": "{}".format(path.join(LOG_DIR_PATH, "git.log")),
                "maxBytes": LOG_MAX,
                "backupCount": LOG_BACKUP_COUNT
            },
            "console": {
                "class" : "logging.StreamHandler",
                "formatter": "{}{}".format("CCSFormatter",LOG_LEVEL),
                "level"   : LOG_LEVEL,
            }
        },
        "loggers":{
            "CCS":{
                "handlers":["fileHandler", "console"], #, "gelfHandler"
                "level"   : LOG_LEVEL
            },
            "GIT":{
                "handlers":["fileHandlerGit", "console"],#, "gelfHandler"
                "level"   : GIT_LOG
            }
        },
        "formatters":{
            "CCSFormatterDEBUG":{
                "format":"%(asctime)s - %(name)s - %(levelname)s - %(message)s  [in %(pathname)s:%(lineno)d]"
            },
            "CCSFormatterINFO":{
                "format":"%(asctime)s - %(name)s - %(levelname)s - %(message)s"
             }
            # "GELFFormatter": {
            #     "class": "logging_gelf.formatters.GELFFormatter"

            # }
        }

    }
    logging.config.dictConfig(LOG_CONF)
    logger = logging.getLogger("CCS")
    logger.info("{}".format("""

    ╔═══╗─────╔═╗────╔═══╗─────╔╗─────╔╗─╔═══╗──────╔╗─────────╔═╦═══╦═══╦═══╦═╗
    ║╔═╗║─────║╔╝────║╔═╗║────╔╝╚╗────║║─║╔═╗║─────╔╝╚╗───────╔╝╔╣╔═╗║╔═╗║╔═╗╠╗╚╗
    ║║─╚╬══╦═╦╝╚╦╦══╗║║─╚╬══╦═╬╗╔╬═╦══╣║─║╚══╦╗─╔╦═╩╗╔╬══╦╗╔╗╔╝╔╝║║─╚╣║─╚╣╚══╗╚╗╚╗
    ║║─╔╣╔╗║╔╬╗╔╬╣╔╗║║║─╔╣╔╗║╔╗╣║║╔╣╔╗║║─╚══╗║║─║║══╣║║║═╣╚╝║║║║─║║─╔╣║─╔╬══╗║─║║║
    ║╚═╝║╚╝║║║║║║║╚╝║║╚═╝║╚╝║║║║╚╣║║╚╝║╚╗║╚═╝║╚═╝╠══║╚╣║═╣║║║║║║─║╚═╝║╚═╝║╚═╝║─║║║
    ╚═══╩══╩╝╚╩╝╚╩═╗║╚═══╩══╩╝╚╩═╩╝╚══╩═╝╚═══╩═╗╔╩══╩═╩══╩╩╩╝╚╗╚╗╚═══╩═══╩═══╝╔╝╔╝
    ─────────────╔═╝║────────────────────────╔═╝║─────────────╚╗╚╗───────────╔╝╔╝
    ─────────────╚══╝────────────────────────╚══╝──────────────╚═╝───────────╚═╝
                                ╔═══╦═══╦═══╦═══╗
                                ║╔═╗║╔═╗║╔═╗║╔═╗║
                                ╚╝╔╝║║║║╠╝╔╝║║║║║
                                ╔═╝╔╣║║║╠═╝╔╣║║║║
                                ║║╚═╣╚═╝║║╚═╣╚═╝║
                                ╚═══╩═══╩═══╩═══╝
    """))
    REPOSITORY = check_repository(Config.UPLOADS, Config.APP_CONFIG, LOG_CONF, logger, GIT, Config.CREDS)
