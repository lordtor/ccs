
# Config Control System (CCS)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=%D0%AE%D1%80%D0%B8%D0%B9-%D0%A0%D1%83%D0%BC%D1%8F%D0%BD%D1%86%D0%B5%D0%B2_ccs&metric=bugs)](https://sonarcloud.io/dashboard?id=%D0%AE%D1%80%D0%B8%D0%B9-%D0%A0%D1%83%D0%BC%D1%8F%D0%BD%D1%86%D0%B5%D0%B2_ccs)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=%D0%AE%D1%80%D0%B8%D0%B9-%D0%A0%D1%83%D0%BC%D1%8F%D0%BD%D1%86%D0%B5%D0%B2_ccs&metric=code_smells)](https://sonarcloud.io/dashboard?id=%D0%AE%D1%80%D0%B8%D0%B9-%D0%A0%D1%83%D0%BC%D1%8F%D0%BD%D1%86%D0%B5%D0%B2_ccs)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=%D0%AE%D1%80%D0%B8%D0%B9-%D0%A0%D1%83%D0%BC%D1%8F%D0%BD%D1%86%D0%B5%D0%B2_ccs&metric=coverage)](https://sonarcloud.io/dashboard?id=%D0%AE%D1%80%D0%B8%D0%B9-%D0%A0%D1%83%D0%BC%D1%8F%D0%BD%D1%86%D0%B5%D0%B2_ccs)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=%D0%AE%D1%80%D0%B8%D0%B9-%D0%A0%D1%83%D0%BC%D1%8F%D0%BD%D1%86%D0%B5%D0%B2_ccs&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=%D0%AE%D1%80%D0%B8%D0%B9-%D0%A0%D1%83%D0%BC%D1%8F%D0%BD%D1%86%D0%B5%D0%B2_ccs)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=%D0%AE%D1%80%D0%B8%D0%B9-%D0%A0%D1%83%D0%BC%D1%8F%D0%BD%D1%86%D0%B5%D0%B2_ccs&metric=ncloc)](https://sonarcloud.io/dashboard?id=%D0%AE%D1%80%D0%B8%D0%B9-%D0%A0%D1%83%D0%BC%D1%8F%D0%BD%D1%86%D0%B5%D0%B2_ccs)

## Blog Posts

An automatic configuration management system for subordinate programs or systems that uses a distributed version control system (Git).

## Quick Start

### First Steps

Installing dependencies

```sh
$ pyvenv-3.6 env
$ source env/bin/activate
$ pip install -r requirements.txt
```

### Configuration

The basic configuration is performed automatically, then it can be edited via the web-ui. You can also configure or add a system via the web-ui. Next, the configuration can be performed by editing the config.json file

### Run

Run each in a different terminal window...

```sh
# the app
$ python run.py
# or
$ flask run
```

## Advanced configuration

| Type    | Section       | Parametr                  | Dscription |
| ------- | ------------- | ------------------------- | ---------- |
| systems | root          | name                      |            |
| systems | git           | cred\_id                  |            |
| systems | git           | url                       |            |
| systems | git           | branch                    |            |
| systems | git           | work\_branch\_pref        |            |
| systems | git           | work\_dir                 |            |
| systems | git           | auto\_recreate            |            |
| systems | git           | auto\_pull                |            |
| app     | root          | SECRET\_KEY               |            |
| app     | root          | CSRF                      |            |
| app     | root          | DEBUG                     |            |
| app     | root          | SESSION\_COOKIE\_SECURE   |            |
| app     | root          | ENV                       |            |
| app     | root          | MAX\_CONTENT\_LENGTH      |            |
| app     | root          | ALLOWED\_FILE\_EXTENSIONS |            |
| app     | root          | uploads\_dir              |            |
| app     | root          | repository\_dir           |            |
| app     | loginig       | log\_dir                  |            |
| app     | loginig       | log\_file\_name           |            |
| app     | loginig       | log\_level                |            |
| app     | loginig       | log\_max\_size            |            |
| app     | loginig       | log\_backup\_count        |            |
| app     | git\_settings | git\_log\_level           |            |
| app     | git\_settings | git\_user                 |            |
| app     | git\_settings | git\_mail                 |            |
| app     | creds         | name                      |            |
| app     | creds         | login                     |            |
| app     | creds         | cred\_file                |            |
